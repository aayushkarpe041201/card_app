import appCSS from "../src/css_folder/App.module.css";

import { useEffect, useState } from "react";

function App() {
  const [cardName, setcardName] = useState("");
  const [CardNumber, setCardNumber] = useState("");
  const [expMonth, setexpMonth] = useState("");
  const [expYear, setexpYear] = useState("");
  const [cvcNum, setcvcNum] = useState("");

  const [error, seterror] = useState(false);

  const [cardNameSub, setcardNameSub] = useState("JANE APPLESEED");
  const [CardNumberSub, setCardNumberSub] = useState("0000 0000 0000 0000");
  const [expDateSub, setexpDateSub] = useState("00/00");
  const [cvcNumSub, setcvcNumSub] = useState("000");

  const onSubmitHandler = (e) => {
    e.preventDefault();

    if (
      cardName.length === 0 ||
      CardNumber === 0 ||
      parseInt(cvcNum) != cvcNum
    ) {
      seterror(true);
    } else {
      setCardNumberSub(CardNumber.match(/.{1,4}/g).join(" "));
      setcardNameSub(cardName);
      setexpDateSub(`${expMonth}/${expYear}`);
      setcvcNumSub(cvcNum);
    }
  };

  return (
    <>
      <div id={appCSS.main}>
        <div id={appCSS.innerSmallLeft}></div>
        <div id={appCSS.innerBigRight}>
          <div id={appCSS.credictFront}>
            <p id={appCSS.accNum}>{CardNumberSub}</p>
            <p id={appCSS.accName}>{cardNameSub}</p>
            <p id={appCSS.accdate}>{expDateSub}</p>
          </div>
          <div id={appCSS.credictBack}>
            <hr id={appCSS.hrLine} />
            <p
              type='text'
              id={appCSS.cardInp}>
              {cvcNumSub}
            </p>
          </div>

          <form
            id='myform'
            action='submit'
            onSubmit={onSubmitHandler}>
            <label
              htmlFor='Cname'
              id={appCSS.CNameLabel}>
              CARDHOLDER NAME
            </label>
            <br />
            <input
              type='text'
              id={appCSS.Cname}
              name='Cname'
              placeholder='e.g. Jane Appleseed'
              value={cardName}
              onChange={(e) => setcardName(e.target.value)}
            />

            {error && cardName.length <= 0 ? (
              <>
                <br />
                <label
                  id={appCSS.err1}
                  htmlFor='Cname'>
                  Cardholder name required
                </label>
              </>
            ) : (
              ""
            )}

            <br />

            <label
              htmlFor='Cnum'
              id={appCSS.CnumLabel}>
              CARD NUMBER
            </label>
            <br />
            <input
              type='tel'
              id={appCSS.Cnum}
              name='Cnum'
              placeholder='e.g. 1234 5678 9123 0000'
              inputMode='numeric'
              pattern='[0-9\s]{13,19}'
              maxLength='19'
              autoComplete='off'
              value={CardNumber}
              onChange={(e) => setCardNumber(e.target.value)}
            />
            <br />

            {error && CardNumber.length <= 0 ? (
              <>
                <label
                  id={appCSS.err2}
                  htmlFor='Cnum'>
                  Card number required
                </label>
                <br />
              </>
            ) : (
              ""
            )}

            <label
              htmlFor='expDate'
              id={appCSS.expDateLabel}>
              EXP.DATE (MM/YY)
            </label>
            <label
              htmlFor='CVC'
              id={appCSS.cvcLabel}>
              CVC
            </label>
            <br />
            <input
              type='text'
              id={appCSS.expDate}
              placeholder='MM'
              value={expMonth}
              onChange={(e) => setexpMonth(e.target.value)}
              required
            />
            <input
              type='text'
              id={appCSS.expMon}
              placeholder='YY'
              value={expYear}
              onChange={(e) => setexpYear(e.target.value)}
              required
            />

            <input
              type='type'
              id={appCSS.cvc}
              placeholder='e.g. 123'
              value={cvcNum}
              onChange={(e) => setcvcNum(e.target.value)}
            />

            {error && parseInt(cvcNum) != cvcNum ? (
              <>
                <br />
                <label
                  id={appCSS.err3}
                  htmlFor='CVC'>
                  CVC must be numeric
                </label>
              </>
            ) : (
              ""
            )}

            <br />

            <button
              type='submit'
              id={appCSS.btn}>
              Confirm
            </button>
          </form>
        </div>
      </div>
    </>
  );
}

export default App;
